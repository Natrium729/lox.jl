# Lox.jl

Lox.jl is an implementation of the language Lox (from the book [*Crafting Interpreters*](https://craftinginterpreters.com/) by Bob Nystrom—go read it, it's great!) in Julia.

More specifically, it implements the tree-walk interpreter from the second part of the book.

## Why Julia?

I chose Julia for the following reasons:

- There were no implementations in Julia yet in [the list of implementations](https://github.com/munificent/craftinginterpreters/wiki/Lox-implementations).
- It was a language I had an interest in learning.
- The book mentioned several times Julia and its multimethods, and it got me curious. (I had heard of Julia before, but not of multimethods.)

It's my first project in Julia, so it's very likely that I've not always used the most idiomatic or efficient code everywhere.

## Differences with the original Java implementation from the book

Lox.jl tries to follow the Java implementation as closely as possible, but differences are obviously unavoidable. Besides trivial ones (syntax, casing, etc.):

- The visitor pattern isn't used, since Julia's multimethods do the same job easily. (Instead of `visitX`, we have several methods with a different type in their signatures, e.g. `evaluate(terp::Interpreter, expr::X)`.)
- Functions and classes related to errors are in a single module, so there is no `RuntimeErrors` module.
- Lexemes are stored as `Symbol`s instead of strings. Quick tests seemed to show it's more efficient (and I think it's more idiomatic anyway).
- There is no equivalent to the `GenerateAst.java` script since structs in Julia don't have the boilerplate that Java needs. The expressions and statements are thus written manually.
- There is no `AstPrinter` module. Julia's function `show` (with a MIME type of "text/plain") is used instead for pretty-printing expressions.
- Julia doesn't have interfaces, so `LoxCallable` is simply an abstract class, with the same functions implemented for each of its subtypes. Also, the types `LoxFunction` and `LoxClass` are in the `LoxCallables` module instead of their own (it's easier to manage like that). Finally, there is a `NativeFunction` class to make it easier to manage native functions such as `clock` (no need to define them in the interpreter constructor).
- The return exception is in `Interpreters` instead of its own module.

## Running the interpreter

You use the interpreter by running the `lox.jl` script at the root of the repository. There are a few options.

### The REPL

```
$ julia lox.jl
```

You can then type Lox statements in your terminal to have them executed.

### Running a Lox script

```
$ julia lox.jl path/to/my/script.lox
```

The script will run with its output printed in stdout and errors in stderr.

### Running the tests

```
$ julia lox.jl test
```

Lox.jl passes all the tests from the original Java implementation. (Except if there are mistakes in my test runner implementation, but there shouldn't be any.)

### Using Lox.jl in another Julia project

In the very unlikely case you need a Lox interpreter in your next Julia project (but hey, we never know, right?), this project is a package so I guess you could import it. Look at the `runstring`, `runfile` and `runprompt` functions in `src/Lox.jl`.

### Benchmarks

There are some benchmarks in the `benchmark` folder. You can run them if you wish, but bear in mind some of them are really slow (especially `fib.lox`, which takes more than 10 minutes on my machine).
