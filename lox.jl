# Run this file with `julia lox.jl [script]` to run a Lox script.

import Pkg
Pkg.activate(@__DIR__; io=devnull)

using Lox

runner = Runner()

if length(ARGS) > 1
    println("Usage: julia lox.jl [script]")
    exit(64)
elseif length(ARGS) == 1
    if ARGS[1] == "test"
        try
            Pkg.test()
        catch
            # Some tests failed. We don't need to do anything.
        end
    else
        try
            exitcode = runfile(runner, ARGS[1])
            exitcode == 0 || exit(exitcode)
        catch e
            # In case the file does not exist, for example.
            showerror(stdout, e)
        end
    end
else
    runprompt(runner)
end
