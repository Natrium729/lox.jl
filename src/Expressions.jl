module Expressions

export LoxExpr, AssignExpr, BinaryExpr, CallExpr, GetExpr, GroupingExpr, LiteralExpr, LogicalExpr, SetExpr, SuperExpr, ThisExpr, UnaryExpr, VariableExpr

using ..Tokens

# We prefix with "Lox" so that there is confusion with Julia's `Expr`s.
abstract type LoxExpr end

# Default to `show` with MIME type of "text/plain" when printing a LoxExpr.
Base.print(io::IO, expr::LoxExpr) = show(io, "text/plain", expr)

#=
Expressions are declared as mutable structs even though they don't change after creation, because they are used as dict keys in the `locals` field of `Interpreter`s; they have to be compared as references for it to work.
=#

mutable struct AssignExpr{T<:LoxExpr} <: LoxExpr
    name::Token
    value::T
end

function Base.show(io::IO, mime::MIME"text/plain", expr::AssignExpr)
    print(io, "(= ", expr.name.lexeme)
    print(io, " ")
    show(io, mime, expr.value)
    print(io, ")")
end


mutable struct BinaryExpr{L<:LoxExpr, R<:LoxExpr} <: LoxExpr
    left::L
    operator::Token
    right::R
end

function Base.show(io::IO, mime::MIME"text/plain", expr::BinaryExpr)
    print(io, "(", expr.operator.lexeme, " ")
    show(io, mime, expr.left)
    print(io, " ")
    show(io, mime, expr.right)
    print(io, ")")
end

mutable struct CallExpr{T<:LoxExpr} <: LoxExpr
    callee::T
    paren::Token
    arguments::Vector{Any}
end

function Base.show(io::IO, mime::MIME"text/plain", expr::CallExpr)
    print(io, "(call ")
    show(io, mime, expr.callee)
    print(io, " [")
    if !isempty(expr.arguments)
        show(io, mime, expr.arguments[1])
        for arg in expr.arguments[2:end]
            print(io, ", ")
            show(io, mime, arg)
        end
    end
    print(io, "])")
end


mutable struct GetExpr{T<:LoxExpr} <: LoxExpr
    object::T
    name::Token
end

function Base.show(io::IO, mime::MIME"text/plain", expr::GetExpr)
    print(io, "(. ")
    show(io, mime, expr.object)
    print(io, " ")
    print(io, expr.name.lexeme)
    print(io, ")")
end


mutable struct GroupingExpr{T<:LoxExpr} <: LoxExpr
    expression::T
end

function Base.show(io::IO, mime::MIME"text/plain", expr::GroupingExpr)
    print(io, "(group ")
    show(io, mime, expr.expression)
    print(io, ")")
end


mutable struct LiteralExpr{T} <: LoxExpr
    value::T
end

Base.show(io::IO, ::MIME"text/plain", expr::LiteralExpr) = print(io, expr.value)
Base.show(io::IO, ::MIME"text/plain", expr::LiteralExpr{Nothing}) = print(io, "nil")
Base.show(io::IO, ::MIME"text/plain", expr::LiteralExpr{<:AbstractString}) =
    print(io, "\"", expr.value, "\"")


mutable struct LogicalExpr{L<:LoxExpr, R<:LoxExpr} <: LoxExpr
    left::L
    operator::Token
    right::R
end

function Base.show(io::IO, mime::MIME"text/plain", expr::LogicalExpr)
    print(io, "(", expr.operator.lexeme, " ")
    show(io, mime, expr.left)
    print(io, " ")
    show(io, mime, expr.right)
    print(io, ")")
end


mutable struct SetExpr{O<:LoxExpr, V<:LoxExpr} <: LoxExpr
    object::O
    name::Token
    value::V
end

function Base.show(io::IO, mime::MIME"text/plain", expr::SetExpr)
    print(io, "(.= ")
    show(io, mime, expr.object)
    print(io, " ")
    print(io, expr.name.lexeme)
    print(io, " ")
    show(io, mime, expr.value)
    print(io, ")")
end


mutable struct SuperExpr <: LoxExpr
    keyword::Token
    method::Token
end

function Base.show(io::IO, ::MIME"text/plain", expr::SuperExpr)
    print(io, "(. ", expr.keyword.lexeme, " ", expr.method.lexeme, ")")
end


mutable struct ThisExpr <: LoxExpr
    keyword::Token
end

function Base.show(io::IO, ::MIME"text/plain", expr::ThisExpr)
    print(io, expr.keyword.lexeme)
end


mutable struct UnaryExpr{T<:LoxExpr} <: LoxExpr
    operator::Token
    right::T
end

function Base.show(io::IO, mime::MIME"text/plain", expr::UnaryExpr)
    print(io, "(", expr.operator.lexeme, " ")
    show(io, mime, expr.right)
    print(io, ")")
end


mutable struct VariableExpr <: LoxExpr
    name::Token
end

function Base.show(io::IO, ::MIME"text/plain", expr::VariableExpr)
    print(io, expr.name.lexeme)
end

end # module
