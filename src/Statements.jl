module Statements

export Stmt, BlockStmt, ClassStmt, ExpressionStmt, FunctionStmt, IfStmt, PrintStmt, ReturnStmt, VarStmt, WhileStmt

using ..Tokens
using ..Expressions

abstract type Stmt end

struct BlockStmt <: Stmt
    statements::Vector{Any}
end

struct ExpressionStmt{T<:LoxExpr} <: Stmt
    expression::T
end

struct FunctionStmt <: Stmt
    name::Token
    params::Vector{Token}
    body::Vector{Any}
end

struct ClassStmt
    name::Token
    superclass::Union{VariableExpr, Nothing}
    methods::Vector{FunctionStmt}
end

struct IfStmt{C<:LoxExpr, T<:Stmt, E<:Union{Stmt, Nothing}} <: Stmt
    condition::C
    thenbranch::T
    elsebranch::E
end

struct PrintStmt{T<:LoxExpr} <: Stmt
    expression::T
end

struct ReturnStmt{T<:Union{<:LoxExpr, Nothing}} <: Stmt
    keyword::Token
    value::T
end

struct VarStmt{T<:Union{<:LoxExpr, Nothing}} <: Stmt
    name::Token
    initialiser::T
end

struct WhileStmt{C<:LoxExpr, T<:Stmt} <: Stmt
    condition::C
    body::T
end

end # module
