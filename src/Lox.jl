module Lox

export Runner, runfile, runstring, runprompt

mutable struct Runner
    io::IO
    ioerr::IO
    haderror::Bool
    hadruntimeerror::Bool
end

Runner() = Runner(stdout, stderr, false, false)
Runner(io::IO, ioerr::IO) = Runner(io, ioerr, false, false)

# These included files don't have nested modules, everything is inside the `Lox` module for the moment.
include("Tokens.jl")
using .Tokens

include("Errors.jl")
using .Errors

include("Scanner.jl")
using .Scanners

include("Expressions.jl")
include("Statements.jl")

include("Parser.jl")
using .Parsers

include("Interpreter.jl")
using .Interpreters

include("Resolvers.jl")
using .Resolvers

function runfile(runner::Runner, p::AbstractString)
    contents = read(p, String)
    terp = Interpreter(runner)
    run(runner, terp, contents)
    runner.haderror && return 65
    runner.hadruntimeerror && return 70
    0
end

function runstring(runner::Runner, contents::AbstractString)
    terp = Interpreter(runner)
    run(runner, terp, contents)
    runner.haderror && return 65
    runner.hadruntimeerror && return 70
    0
end

function runprompt(runner::Runner)
    terp = Interpreter(runner)
    while true
        print("lox>")
        line = readline();
        run(runner, terp, line)
        println()
        runner.haderror = false
    end
end

function run(runner::Runner, terp, source::AbstractString)
    scanner = Scanner(runner, source)
    tokens = scantokens(scanner)
    parser = Parser(runner, tokens)
    statements = parser()

    # Stop if there was a syntax error.
    runner.haderror && return

    resolver = Resolver(terp)
    resolve(resolver, statements)

    # Stop if there was a resolving error.
    runner.haderror && return

    interpret(terp, statements)
end

end # module
