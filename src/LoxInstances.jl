struct LoxInstance
    class::LoxClass
    fields::Dict{Symbol, Any}
end

LoxInstance(class::LoxClass) = LoxInstance(class, Dict())

Base.show(io::IO, instance::LoxInstance) = print(io, instance.class.name, " instance")

function getprop(object::LoxInstance, name::Token)
    if haskey(object.fields, name.lexeme)
        return object.fields[name.lexeme]
    end

    method = findmethod(object.class, name.lexeme)
    method !== nothing && return bindmethod(method, object)

    throw(RuntimeError(name, "Undefined property '$(name.lexeme)'."))
end

function setprop(object::LoxInstance, name::Token, value)
    object.fields[name.lexeme] = value
end
