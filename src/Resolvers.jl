module Resolvers

export Resolver, resolve

using ..Lox
using ..Tokens
using ..Errors
using ..Expressions
using ..Statements
using ..Interpreters

@enum FunctionType begin
    ftype_none
    ftype_function
    ftype_initialiser
    ftype_method
end

@enum ClassType begin
    ctype_none
    ctype_class
    ctype_subclass
end

mutable struct Resolver
    interpreter::Interpreter
    scopes::Array{Dict{Symbol, Bool}}
    currentfunction::FunctionType
    currentclass::ClassType
end

Resolver(terp::Interpreter) = Resolver(terp, [], ftype_none, ctype_none)

function resolve(resolver::Resolver, list::AbstractArray)
    for elem in list
        resolve(resolver, elem)
    end
end

function resolve(resolver::Resolver, stmt::BlockStmt)
    beginscope(resolver)
    resolve(resolver, stmt.statements)
    endscope(resolver)
end

function resolve(resolver::Resolver, stmt::ClassStmt)
    enclosingclass = resolver.currentclass
    resolver.currentclass = ctype_class

    declare(resolver, stmt.name)
    define(resolver, stmt.name)

    if stmt.superclass !== nothing
        if stmt.name.lexeme === stmt.superclass.name.lexeme
            loxerror(resolver.interpreter.runner, stmt.superclass.name, "A class can't inherit from itself.")
        else
            resolver.currentclass = ctype_subclass
            resolve(resolver, stmt.superclass)
        end

        beginscope(resolver)
        resolver.scopes[end][:super] = true
    end


    beginscope(resolver)
    last(resolver.scopes)[:this] = true

    for method in stmt.methods
        declaration = method.name.lexeme === :init ? ftype_initialiser : ftype_method
        resolvefunction(resolver, method, declaration)
    end

    endscope(resolver)

    stmt.superclass !== nothing && endscope(resolver)

    resolver.currentclass = enclosingclass
end

function resolve(resolver::Resolver, stmt::ExpressionStmt)
    resolve(resolver, stmt.expression)
end

function resolve(resolver::Resolver, stmt::FunctionStmt)
    declare(resolver, stmt.name)
    define(resolver, stmt.name)
    resolvefunction(resolver, stmt, ftype_function)
end

function resolve(resolver::Resolver, stmt::IfStmt)
    resolve(resolver, stmt.condition)
    resolve(resolver, stmt.thenbranch)
    stmt.elsebranch !== nothing && resolve(resolver, stmt.elsebranch)
end

function resolve(resolver::Resolver, stmt::PrintStmt)
    resolve(resolver, stmt.expression)
end

function resolve(resolver::Resolver, stmt::ReturnStmt)
    if resolver.currentfunction == ftype_none
        loxerror(resolver.interpreter.runner, stmt.keyword, "Can't return from top-level code.")
    end
    if stmt.value !== nothing
        if resolver.currentfunction == ftype_initialiser
            loxerror(resolver.interpreter.runner, stmt.keyword, "Can't return a value from an initializer.")
        end
        resolve(resolver, stmt.value)
    end
end

function resolve(resolver::Resolver, stmt::VarStmt)
    declare(resolver, stmt.name)
    stmt.initialiser !== nothing && resolve(resolver, stmt.initialiser)
    define(resolver, stmt.name)
end

function resolve(resolver::Resolver, stmt::WhileStmt)
    resolve(resolver, stmt.condition)
    resolve(resolver, stmt.body)
end

function resolve(resolver::Resolver, expr::AssignExpr)
    resolve(resolver, expr.value)
    resolvelocal(resolver, expr, expr.name)
end

function resolve(resolver::Resolver, expr::BinaryExpr)
    resolve(resolver, expr.left)
    resolve(resolver, expr.right)
end

function resolve(resolver::Resolver, expr::CallExpr)
    resolve(resolver, expr.callee)
    for argument in expr.arguments
        resolve(resolver, argument)
    end
end

function resolve(resolver::Resolver, expr::GetExpr)
    resolve(resolver, expr.object)
end

function resolve(resolver::Resolver, expr::GroupingExpr)
    resolve(resolver, expr.expression)
end

function resolve(::Resolver, ::LiteralExpr) end

function resolve(resolver::Resolver, expr::LogicalExpr)
    resolve(resolver, expr.left)
    resolve(resolver, expr.right)
end

function resolve(resolver::Resolver, expr::SetExpr)
    resolve(resolver, expr.value)
    resolve(resolver, expr.object)
end

function resolve(resolver::Resolver, expr::SuperExpr)
    if resolver.currentclass === ctype_none
        loxerror(resolver.interpreter.runner, expr.keyword, "Can't use 'super' outside of a class.")
    elseif resolver.currentclass !== ctype_subclass
        loxerror(resolver.interpreter.runner, expr.keyword, "Can't use 'super' in a class with no superclass.")
    end
    resolvelocal(resolver, expr, expr.keyword)
end

function resolve(resolver::Resolver, expr::ThisExpr)
    if resolver.currentclass == ctype_none
        loxerror(resolver.interpreter.runner, expr.keyword, "Can't use 'this' outside of a class.")
        return
    end
    resolvelocal(resolver, expr, expr.keyword)
end

function resolve(resolver::Resolver, expr::UnaryExpr)
    resolve(resolver, expr.right)
end

function resolve(resolver::Resolver, expr::VariableExpr)
    if !isempty(resolver.scopes) && haskey(last(resolver.scopes), expr.name.lexeme) && !last(resolver.scopes)[expr.name.lexeme]
        loxerror(resolver.interpreter.runner, expr.name, "Can't read local variable in its own initializer.")
    end

    resolvelocal(resolver, expr, expr.name)
end

function resolvefunction(resolver::Resolver, fun::FunctionStmt, type::FunctionType)
    enclosingfunction = resolver.currentfunction
    resolver.currentfunction = type
    beginscope(resolver)
    for param in fun.params
        declare(resolver, param)
        define(resolver, param)
    end
    resolve(resolver, fun.body)
    endscope(resolver)
    resolver.currentfunction = enclosingfunction
end

beginscope(resolver::Resolver) = push!(resolver.scopes, Dict())

endscope(resolver::Resolver) = pop!(resolver.scopes)

function declare(resolver::Resolver, name::Token)
    isempty(resolver.scopes) && return
    scope = last(resolver.scopes)
    if haskey(scope, name.lexeme)
        loxerror(resolver.interpreter.runner, name, "Already a variable with this name in this scope.")
    end
    scope[name.lexeme] = false
end

function define(resolver::Resolver, name::Token)
    isempty(resolver.scopes) && return
    last(resolver.scopes)[name.lexeme] = true
end

function resolvelocal(resolver::Resolver, expr::LoxExpr, name::Token)
    scopeslength = length(resolver.scopes)
    for i = scopeslength:-1:1
        if haskey(resolver.scopes[i], name.lexeme)
            Interpreters.resolve(resolver.interpreter, expr, convert(UInt, scopeslength - i))
            return
        end
    end
end

end # module
