module Interpreters

export Interpreter, interpret

using Printf

using ..Lox
using ..Errors
using ..Tokens
using ..Expressions
using ..Statements

include("Environments.jl")

mutable struct Interpreter
    runner::Runner
    globals::Environment
    environment::Environment
    locals::Dict{LoxExpr, UInt}
    function Interpreter(runner::Runner, globals::Environment)
        defineval(globals, :clock, NativeFunction(:clock))
        new(runner, globals, globals, Dict())
    end
end

Interpreter(runner::Runner) = (env = Environment(); Interpreter(runner, env))

include("LoxCallables.jl")
include("LoxInstances.jl")

struct Return{T} <: Exception
    value::T
end

function interpret(terp::Interpreter, statements::AbstractVector)
    try
        for statement in statements
            execute(terp, statement)
        end
    catch e
        if e isa RuntimeError
            runtimeerror(terp.runner, e)
        else
            rethrow()
        end
    end
end

resolve(terp::Interpreter, expr::LoxExpr, depth::UInt) = (terp.locals[expr] = depth)

execute(terp::Interpreter, stmt::ExpressionStmt) = evaluate(terp, stmt.expression)

function execute(terp::Interpreter, stmt::ClassStmt)
    superclass = nothing
    if stmt.superclass !== nothing
        superclass = evaluate(terp, stmt.superclass)
        if !isa(superclass, LoxClass)
            throw(RuntimeError(stmt.superclass.name, "Superclass must be a class."))
        end
    end

    defineval(terp.environment, stmt.name.lexeme, nothing)

    if stmt.superclass !== nothing
        terp.environment = Environment(terp.environment)
        defineval(terp.environment, :super, superclass)
    end

    methods = Dict{Symbol, LoxFunction}()
    for method in stmt.methods
        methods[method.name.lexeme] = LoxFunction(method, terp.environment, method.name.lexeme === :init)
    end

    class = LoxClass(stmt.name.lexeme, superclass, methods)

    if stmt.superclass !== nothing
        terp.environment = terp.environment.enclosing
    end

    assignval(terp.environment, stmt.name, class)
end

function execute(terp::Interpreter, stmt::FunctionStmt)
    fun = LoxFunction(stmt, terp.environment, false)
    defineval(terp.environment, stmt.name.lexeme, fun)
    nothing
end

function execute(terp::Interpreter, stmt::IfStmt)
    if istruthy(evaluate(terp, stmt.condition))
        execute(terp, stmt.thenbranch)
    elseif stmt.elsebranch !== nothing
        execute(terp, stmt.elsebranch)
    end
end

function execute(terp::Interpreter, stmt::PrintStmt)
    value = evaluate(terp, stmt.expression)
    println(terp.runner.io, stringify(value))
end

function execute(terp::Interpreter, stmt::ReturnStmt)
    value = stmt.value === nothing ? nothing : evaluate(terp, stmt.value)
    throw(Return(value))
end

function execute(terp::Interpreter, stmt::VarStmt)
    value = stmt.initialiser === nothing ? nothing : evaluate(terp, stmt.initialiser)
    defineval(terp.environment, stmt.name.lexeme, value)
end

function execute(terp::Interpreter, stmt::WhileStmt)
    while istruthy(evaluate(terp, stmt.condition))
        execute(terp, stmt.body)
    end
end

function execute(terp::Interpreter, stmt::BlockStmt)
    executeblock(terp, stmt.statements, Environment(terp.environment))
end

function executeblock(terp::Interpreter, statements::AbstractVector, newenv::Environment)
    previous = terp.environment
    try
        terp.environment = newenv
        for statement in statements
            execute(terp, statement)
        end
    finally
        terp.environment = previous
    end
end

function evaluate(terp::Interpreter, expr::AssignExpr)
    value = evaluate(terp, expr.value)
    if haskey(terp.locals, expr)
        assignvalat(terp.environment, terp.locals[expr], expr.name, value)
    else
        assignval(terp.globals, expr.name, value)
    end
    value
end

function evaluate(terp::Interpreter, expr::BinaryExpr)
    left = evaluate(terp, expr.left)
    right = evaluate(terp, expr.right)

    type = expr.operator.type
    if type == token_greater
        checknumberoperands(expr.operator, left, right)
        return left > right
    elseif type == token_greaterequal
        checknumberoperands(expr.operator, left, right)
        return left >= right
    elseif type == token_less
        checknumberoperands(expr.operator, left, right)
        return left < right
    elseif type == token_lessequal
        checknumberoperands(expr.operator, left, right)
        return left <= right
    elseif type == token_bangequal
        return !isequal(left, right)
    elseif type == token_equalequal
        return isequal(left, right)
    elseif type == token_minus
        checknumberoperands(expr.operator, left, right)
        return left - right
    elseif type == token_plus
        left isa AbstractFloat && right isa AbstractFloat && return left + right
        left isa AbstractString && right isa AbstractString && return left * right
        throw(RuntimeError(expr.operator, "Operands must be two numbers or two strings."))
    elseif type == token_slash
        checknumberoperands(expr.operator, left, right)
        return left / right
    elseif type == token_star
        checknumberoperands(expr.operator, left, right)
        return left * right
    end

    error("Unreachable code.")
end

function evaluate(terp::Interpreter, expr::CallExpr)
    callee = evaluate(terp, expr.callee)
    arguments = []
    for argument in expr.arguments
        push!(arguments, evaluate(terp, argument))
    end

    if !isa(callee, LoxCallable)
        throw(RuntimeError(expr.paren, "Can only call functions and classes."))
    end

    fn_arity = arity(callee)
    if length(arguments) != fn_arity
        throw(RuntimeError(expr.paren, "Expected $(fn_arity) arguments but got $(length(arguments))."))
    end
    return call(terp, callee, arguments)
end

function evaluate(terp::Interpreter, expr::GetExpr)
    object = evaluate(terp, expr.object)
    if object isa LoxInstance
        return getprop(object, expr.name)
    end

    throw(RuntimeError(expr.name, "Only instances have properties."))
end

evaluate(terp::Interpreter, expr::GroupingExpr) = evaluate(terp, expr.expression)

evaluate(::Interpreter, expr::LiteralExpr) = expr.value

function evaluate(terp::Interpreter, expr::LogicalExpr)
    left = evaluate(terp, expr.left)

    if expr.operator.type == token_or
        istruthy(left) && return left
    else
        istruthy(left) || return left
    end

    evaluate(terp, expr.right)
end

function evaluate(terp::Interpreter, expr::SetExpr)
    object = evaluate(terp, expr.object)

    object isa LoxInstance || throw(RuntimeError(expr.name, "Only instances have fields."))

    value = evaluate(terp, expr.value)
    setprop(object, expr.name, value)

    value
end

function evaluate(terp::Interpreter, expr::SuperExpr)
    distance = terp.locals[expr]
    superclass = getvalat(terp.environment, distance, :super)
    object = getvalat(terp.environment, distance - 1, :this)

    method = findmethod(superclass, expr.method.lexeme)

    if method === nothing
        throw(RuntimeError(expr.method, "Undefined property '$(expr.method.lexeme)'."))
    end

    return bindmethod(method, object)
end

evaluate(terp::Interpreter, expr::ThisExpr) = lookupvariable(terp, expr.keyword, expr)

function evaluate(terp::Interpreter, expr::UnaryExpr)
    right = evaluate(terp, expr.right)

    type = expr.operator.type
    if type == token_minus
        checknumberoperand(expr.operator, right)
        return -right
    elseif type == token_bang
        return !istruthy(right)
    end

    Base.error("Unreachable code.")
end

evaluate(terp::Interpreter, expr::VariableExpr) = lookupvariable(terp, expr.name, expr)

function lookupvariable(terp::Interpreter, name::Token, expr::LoxExpr)
    if haskey(terp.locals, expr)
        return getvalat(terp.environment, terp.locals[expr], name.lexeme)
    else
        return getval(terp.globals, name)
    end
end

function checknumberoperand(operator::Token, operand)
    operand isa AbstractFloat || throw(RuntimeError(operator, "Operand must be a number."))
end

function checknumberoperands(operator::Token, left, right)
    left isa AbstractFloat && right isa AbstractFloat ||
    throw(RuntimeError(operator, "Operands must be numbers."))
end

function istruthy(object)
    object === nothing && return false
    object isa Bool && return object
    true
end

function isequal(left, right)
    typeof(left) == typeof(right) || return false
    left == right
end

function stringify(object)
    object === nothing && return "nil"
    stringified = string(object)
    if object isa AbstractFloat && endswith(stringified, ".0")
        return Printf.@sprintf("%.0f", object)
    end
    stringified
end

end # module
