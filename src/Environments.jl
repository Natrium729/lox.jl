struct Environment
    enclosing::Union{Environment, Nothing}
    values::Dict{Symbol, Any}
end

Environment() = Environment(nothing, Dict())

Environment(enclosing::Environment) = Environment(enclosing, Dict())

function defineval(env::Environment, name::Symbol, value)
    env.values[name] = value
end

function ancestor(env::Environment, distance::UInt)
    target = env
    for i = 1:distance
        target = target.enclosing
    end
    target
end

function getvalat(env::Environment, distance::UInt, name::Symbol)
    ancestor(env, distance).values[name]
end

function assignvalat(env::Environment, distance::UInt, name::Token, value)
    ancestor(env, distance).values[name.lexeme] = value
end

function getval(env::Environment, name::Token)
    haskey(env.values, name.lexeme) && return env.values[name.lexeme]
    env.enclosing !== nothing && return getval(env.enclosing, name)

    throw(RuntimeError(name, "Undefined variable '$(name.lexeme)'."))
end

function assignval(env::Environment, name::Token, value)
    if haskey(env.values, name.lexeme)
        env.values[name.lexeme] = value
        return
    end

    if env.enclosing !== nothing
        assignval(env.enclosing, name, value)
        return
    end

    throw(RuntimeError(name, "Undefined variable '$(name.lexeme)'."))
end
