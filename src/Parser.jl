module Parsers

export Parser

using ..Lox
using ..Errors
using ..Tokens
using ..Expressions
using ..Statements

mutable struct Parser
    runner::Runner
    tokens::Vector{Token}
    current::UInt
end

Parser(runner, tokens) = Parser(runner, tokens, one(UInt))

struct ParseError <: Exception end

# Call the parser instance to parse its tokens.
# program → declaration* EOF ;
function (p::Parser)()
    statements = []
    while !isatend(p)
        push!(statements, declaration(p))
    end
    statements
end

# declaration → classDecl | funDecl | varDecl | statement ;
function declaration(parser::Parser)
    try
        match(parser, token_class) && return classdeclaration(parser)
        match(parser, token_fun) && return fundeclaration(parser, :function)
        match(parser, token_var) && return vardeclaration(parser)
        return statement(parser)
    catch e
        if e isa ParseError
            synchronise(parser)
        else
            rethrow()
        end
    end
end

# funDecl → "fun" function ;
# function → IDENTIFIER "(" parameters? ")" block ;
# parameters → IDENTIFIER ( "," IDENTIFIER )* ;
function fundeclaration(parser::Parser, kind::Symbol)
    name = consume(parser, token_identifier, "Expect $kind name.")

    consume(parser, token_leftparen, "Expect '(' after $kind name.")
    parameters = []
    if !check(parser, token_rightparen)
        while true
            if length(parameters) >= 255
                parseerror(parser, peek(parser), "Can't have more than 255 parameters.")
            end
            push!(parameters, consume(parser, token_identifier, "Expect parameter name."))
            match(parser, token_comma) || break
        end
    end
    consume(parser, token_rightparen, "Expect ')' after parameters.")

    consume(parser, token_leftbrace, "Expect '{' before $kind body.")
    body = block(parser)

    FunctionStmt(name, parameters, body)
end

# classDecl → "class" IDENTIFIER ( "<" IDENTIFIER )? "{" function* "}" ;
function classdeclaration(parser::Parser)
    name = consume(parser, token_identifier, "Expect class name.")

    superclass = nothing
    if match(parser, token_less)
        consume(parser, token_identifier, "Expect superclass name.")
        superclass = VariableExpr(previous(parser))
    end

    consume(parser, token_leftbrace, "Expect '{' before class body.")
    methods = []
    while !check(parser, token_rightbrace) && !isatend(parser)
        push!(methods, fundeclaration(parser, :method))
    end
    consume(parser, token_rightbrace, "Expect '}' after class body.")

    ClassStmt(name, superclass, methods)
end

# varDecl → "var" IDENTIFIER ( "=" expression )? ";" ;
function vardeclaration(parser::Parser)
    name = consume(parser, token_identifier, "Expect variable name.")

    initialiser = nothing
    if match(parser, token_equal)
        initialiser = expression(parser)
    end

    consume(parser, token_semicolon, "Expect ';' after variable declaration.")
    VarStmt(name, initialiser)
end

# statement → exprStmt | forStmt | ifStmt | printStmt | returnStmt | whileStmt | block ;
function statement(parser::Parser)
    match(parser, token_for) && return forstatement(parser)
    match(parser, token_if) && return ifstatement(parser)
    match(parser, token_print) && return printstatement(parser)
    match(parser, token_return) && return returnstatement(parser)
    match(parser, token_while) && return whilestatement(parser)
    match(parser, token_leftbrace) && return BlockStmt(block(parser))
    expressionstatement(parser)
end

# forStmt → "for" "(" ( varDecl | exprStmt | ";" ) expression? ";" expression? ")" statement ;
function forstatement(parser::Parser)
    consume(parser, token_leftparen, "Expect '(' after 'for'.")

    initialiser = nothing
    if match(parser, token_var)
        initialiser = vardeclaration(parser)
    elseif !match(parser, token_semicolon)
        initialiser = expressionstatement(parser)
    end

    condition = nothing
    if !check(parser, token_semicolon)
        condition = expression(parser)
    end
    consume(parser, token_semicolon, "Expect ';' after loop condition.")

    increment = nothing
    if !check(parser, token_rightparen)
        increment = expression(parser)
    end
    consume(parser, token_rightparen, "Expect ')' after for clauses.")

    body = statement(parser)

    increment !== nothing && (body = BlockStmt([body, ExpressionStmt(increment)]))

    condition === nothing && (condition = LiteralExpr(true))
    body = WhileStmt(condition, body)

    initialiser !== nothing && (body = BlockStmt([initialiser, body]))

    body
end

# ifStmt → "if" "(" expression ")" statement ( "else" statement )? ;
function ifstatement(parser::Parser)
    consume(parser, token_leftparen, "Expect '(' after 'if'.")
    condition = expression(parser)
    consume(parser, token_rightparen, "Expect ')' after if condition.")

    thenbranch = statement(parser)
    elsebranch = match(parser, token_else) ? elsebranch = statement(parser) : nothing

    IfStmt(condition, thenbranch, elsebranch)
end

# printStmt → "print" expression ";" ;
function printstatement(parser::Parser)
    value = expression(parser)
    consume(parser, token_semicolon, "Expect ';' after value.")
    PrintStmt(value)
end

# returnStmt → "return" expression? ";" ;
function returnstatement(parser::Parser)
    keyword = previous(parser)
    value = nothing
    if !check(parser, token_semicolon)
        value = expression(parser)
    end
    consume(parser, token_semicolon, "Expect ';' after return value.")

    ReturnStmt(keyword, value)
end

# whileStmt → "while" "(" expression ")" statement ;
function whilestatement(parser::Parser)
    consume(parser, token_leftparen, "Expect '(' after 'while'.")
    condition = expression(parser)
    consume(parser, token_rightparen, "Expect ')' after condition.")
    body = statement(parser)

    WhileStmt(condition, body)
end

# exprStmt → expression ";" ;
function expressionstatement(parser::Parser)
    expr = expression(parser)
    consume(parser, token_semicolon, "Expect ';' after expression.")
    ExpressionStmt(expr)
end

# block → "{" declaration* "}" ;
function block(parser::Parser)
    statements = []

    while !check(parser, token_rightbrace) && !isatend(parser)
        push!(statements, declaration(parser))
    end
    consume(parser, token_rightbrace, "Expect '{' after block.")

    statements
end

# expression → assignment ;
expression(parser::Parser) = assignment(parser)

# assignment → ( call "." )? IDENTIFIER "=" assignment| logic_or ;
function assignment(parser::Parser)
    expr = or(parser)

    if match(parser, token_equal)
        equals = previous(parser)
        value = assignment(parser)

        if expr isa VariableExpr
            name = expr.name
            return AssignExpr(name, value)
        elseif expr isa GetExpr
            return SetExpr(expr.object, expr.name, value)
        end

        parseerror(parser, equals, "Invalid assignment target.")
    end

    expr
end

# TODO: Make a macro to generate functions parsing left-associative binary expressions?
# There would be less copy-paste.

# logic_or → logic_and ( "or" logic_and )* ;
function or(parser::Parser)
    expr = and(parser)
    while match(parser, token_or)
        operator = previous(parser)
        right = and(parser)
        expr = LogicalExpr(expr, operator, right)
    end

    expr
end

# logic_and → equality ( "and" equality )* ;
function and(parser::Parser)
    expr = equality(parser)
    while match(parser, token_and)
        operator = previous(parser)
        right = equality(parser)
        expr = LogicalExpr(expr, operator, right)
    end

    expr
end

# equality → comparison ( ( "!=" | "==" ) comparison )* ;
function equality(parser::Parser)
    expr = comparison(parser)

    while match(parser, token_bangequal, token_equalequal)
        operator = previous(parser)
        right = comparison(parser)
        expr = BinaryExpr(expr, operator, right)
    end

    expr
end

# comparison → term ( ( ">" | ">=" | "<" | "<=" ) term )* ;
function comparison(parser::Parser)
    expr = term(parser)

    while match(parser, token_greater, token_greaterequal, token_less, token_lessequal)
        operator = previous(parser)
        right = term(parser)
        expr = BinaryExpr(expr, operator, right)
    end

    expr
end

# term → factor ( ( "-" | "+" ) factor )* ;
function term(parser::Parser)
    expr = factor(parser)

    while match(parser, token_minus, token_plus)
        operator = previous(parser)
        right = factor(parser)
        expr = BinaryExpr(expr, operator, right)
    end

    expr
end

# factor → unary ( ( "/" | "*" ) unary )* ;
function factor(parser::Parser)
    expr = unary(parser)

    while match(parser, token_slash, token_star)
        operator = previous(parser)
        right = unary(parser)
        expr = BinaryExpr(expr, operator, right)
    end

    expr
end

# unary → ( "!" | "-" ) unary | call ;
function unary(parser::Parser)
    if match(parser, token_bang, token_minus)
        operator = previous(parser)
        right = unary(parser)
        return UnaryExpr(operator, right)
    end

    call(parser)
end

# call → primary ( "(" arguments? ")" | "." IDENTIFIER )* ;
function call(parser::Parser)
    expr = primary(parser)

    while true
        if match(parser, token_leftparen)
            expr = finishcall(parser, expr)
        elseif match(parser, token_dot)
            name = consume(parser, token_identifier, "Expect property name after '.'.")
            expr = GetExpr(expr, name)
        else
            break
        end
    end

    expr
end

# arguments → expression ( "," expression )* ;
function finishcall(parser::Parser, callee::LoxExpr)
    arguments = []
    if !check(parser, token_rightparen)
        while true
            if length(arguments) >= 255
                parseerror(parser, peek(parser), "Can't have more than 255 arguments.")
            end
            push!(arguments, expression(parser))
            match(parser, token_comma) || break
        end
    end

    paren = consume(parser, token_rightparen, "Expect ')' after arguments.")
    CallExpr(callee, paren, arguments)
end

# primary → "true" | "false" | "nil" | "this" | NUMBER | STRING | IDENTIFIER | "(" expression ")" | "super" "." IDENTIFIER ;
function primary(parser::Parser)
    match(parser, token_false) && return LiteralExpr(false)
    match(parser, token_true) && return LiteralExpr(true)
    match(parser, token_nil) && return LiteralExpr(nothing)

    if match(parser, token_number, token_string)
        return LiteralExpr(previous(parser).literal)
    end

    if match(parser, token_super)
        keyword = previous(parser)
        consume(parser, token_dot, "Expect '.' after 'super'.")
        method = consume(parser, token_identifier, "Expect superclass method name.")
        return SuperExpr(keyword, method)
    end
    match(parser, token_identifier) && return VariableExpr(previous(parser))

    match(parser, token_this) && return ThisExpr(previous(parser))

    if match(parser, token_leftparen)
        expr = expression(parser)
        consume(parser, token_rightparen, "Expect ')' after expression.")
        return GroupingExpr(expr)
    end

    throw(parseerror(parser, peek(parser), "Expect expression."))
end

function match(parser::Parser, types::Vararg{TokenType})
    for type in types
        if check(parser, type)
            advance(parser)
            return true
        end
    end

    false
end

function consume(parser::Parser, type::TokenType, message::AbstractString)
    check(parser, type) && return advance(parser)

    throw(parseerror(parser, peek(parser), message))
end

function check(parser::Parser, type::TokenType)
    isatend(parser::Parser) && return false
    peek(parser).type == type
end

function advance(parser::Parser)
    isatend(parser) || (parser.current += 1)
    previous(parser)
end

isatend(parser::Parser) = peek(parser).type == token_eof

peek(parser::Parser) = parser.tokens[parser.current]

previous(parser::Parser) = parser.tokens[parser.current - 1]

function parseerror(parser::Parser, token::Token, message::AbstractString)::ParseError
    loxerror(parser.runner, token, message)
    ParseError()
end

function synchronise(parser::Parser)
    advance(parser)

    while !isatend(parser)
        previous(parser).type == token_semicolon && return

        current = peek(parser).type
        if current == token_class || current == token_fun || current == token_var || current == token_for || current == token_if || current == token_while || current == token_print || current == token_return
            return
        end

        advance(parser)
    end
end

end # module
