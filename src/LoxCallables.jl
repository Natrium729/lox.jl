using Dates

abstract type LoxCallable end


struct NativeFunction <: LoxCallable
    id::Symbol
end

Base.show(io::IO, fun::NativeFunction) = print(io, "<native fun ", fun.id, ">")

function arity(callee::NativeFunction)
    id = callee.id
    if id === :clock
        return 0
    else
        error("Interpreter error: unknown native function.")
    end
end

function call(::Interpreter, callee::NativeFunction, arguments::AbstractVector)
    id = callee.id
    if id === :clock
        return callclock()
    else
        error("Interpreter error: unknown native function.")
    end
end

callclock() = datetime2unix(now())


struct LoxFunction <: LoxCallable
    declaration::FunctionStmt
    closure::Environment
    isinitialiser::Bool
end

function Base.show(io::IO, fun::LoxFunction)
    print(io, "<fun ", fun.declaration.name.lexeme, ">")
end

arity(callee::LoxFunction) = length(callee.declaration.params)

function call(terp::Interpreter, callee::LoxFunction, arguments::AbstractVector)
    environment = Environment(callee.closure)
    for i = 1:length(callee.declaration.params)
        defineval(environment, callee.declaration.params[i].lexeme, arguments[i])
    end

    try
        executeblock(terp, callee.declaration.body, environment)
    catch e
        if e isa Return
            return callee.isinitialiser ? getvalat(callee.closure, zero(UInt), :this) : e.value
        end
        rethrow()
    end

    callee.isinitialiser && return getvalat(callee.closure, zero(UInt), :this)

    nothing
end

# `instance` is any because the `LoxInstance` type is defined later.
# I could move the function to `LoxInstances.jl` but I want to stay as close as possible to jlox's structure.
function bindmethod(fun::LoxFunction, instance)
    instance::LoxInstance
    environment = Environment(fun.closure)
    defineval(environment, :this, instance)
    LoxFunction(fun.declaration, environment, fun.isinitialiser)
end


struct LoxClass <: LoxCallable
    name::Symbol
    superclass::Union{LoxClass, Nothing}
    methods::Dict{Symbol, LoxFunction}
end

Base.show(io::IO, class::LoxClass) = print(io, class.name)

function arity(callee::LoxClass)
    initialiser = findmethod(callee, :init)
    initialiser === nothing ? 0 : arity(initialiser)
end

function call(terp::Interpreter, callee::LoxClass, arguments::AbstractVector)
    instance = LoxInstance(callee)
    initialiser = findmethod(callee, :init)
    initialiser !== nothing && call(terp, bindmethod(initialiser, instance), arguments)
    instance
end

function findmethod(class::LoxClass, name::Symbol)
    haskey(class.methods, name) && return class.methods[name]

    class.superclass !== nothing && return findmethod(class.superclass, name)

    nothing
end
