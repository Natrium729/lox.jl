module Errors

export loxerror, RuntimeError, runtimeerror

using ..Lox
using ..Tokens

struct RuntimeError{T<:Token, S<:AbstractString} <: Exception
    token::T
    msg::S
end

loxerror(runner::Runner, line, message) = reporterror(runner, line, "", message)

function loxerror(runner::Runner, token::Token, message::AbstractString)
    loc = token.type == token_eof ? " at end" : " at '$(token.lexeme)'"
    reporterror(runner, token.line, loc, message)
end

function reporterror(runner, line, loc, message::AbstractString)
    println(runner.ioerr, "[line ", line, "] Error", loc, ": ", message)
    runner.haderror = true
end

function runtimeerror(runner::Runner, e::RuntimeError)
    println(runner.ioerr, e.msg, "\n[line ", e.token.line, "]")
    runner.hadruntimeerror = true
end

end # module
