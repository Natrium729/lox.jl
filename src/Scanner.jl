module Scanners

export Scanner, scantokens

using ..Lox
using ..Tokens
using ..Errors

mutable struct Scanner{S<:AbstractString}
    runner::Runner
    source::S
    tokens::Vector{Any}
    start::UInt
    current::UInt
    line::UInt
end

Scanner(runner, source) = Scanner(runner, source, [], one(UInt), one(UInt), one(UInt))

nextchar(scanner::Scanner) = (scanner.current = nextind(scanner.source, scanner.current))

const keywords = Dict(
    "and" => token_and,
    "class" => token_class,
    "else" => token_else,
    "false" => token_false,
    "for" => token_for,
    "fun" => token_fun,
    "if" => token_if,
    "nil" => token_nil,
    "or" => token_or,
    "print" => token_print,
    "return" => token_return,
    "super" => token_super,
    "this" => token_this,
    "true" => token_true,
    "var" => token_var,
    "while" => token_while,
)

function scantokens(scanner::Scanner)
    while !isatend(scanner)
        scanner.start = scanner.current
        scantoken(scanner)
    end

    push!(scanner.tokens, Token(token_eof, Symbol(""), nothing, scanner.line))
end

function scantoken(scanner::Scanner)
    char = advance(scanner)
    if char == '('
        addtoken(scanner, token_leftparen);
    elseif char == ')'
        addtoken(scanner, token_rightparen);
    elseif char == '{'
        addtoken(scanner, token_leftbrace);
    elseif char == '}'
        addtoken(scanner, token_rightbrace);
    elseif char == ','
        addtoken(scanner, token_comma);
    elseif char == '.'
        addtoken(scanner, token_dot);
    elseif char == '-'
        addtoken(scanner, token_minus);
    elseif char == '+'
        addtoken(scanner, token_plus);
    elseif char == ';'
        addtoken(scanner, token_semicolon);
    elseif char == '*'
        addtoken(scanner, token_star);
    elseif char == '!'
        addtoken(scanner, match(scanner, '=') ? token_bangequal : token_bang)
    elseif char == '='
        addtoken(scanner, match(scanner, '=') ? token_equalequal : token_equal)
    elseif char == '<'
        addtoken(scanner, match(scanner, '=') ? token_lessequal : token_less)
    elseif char == '>'
        addtoken(scanner, match(scanner, '=') ? token_greaterequal : token_greater)
    elseif char == '/'
        if match(scanner, '/')
            while peek(scanner) != '\n' && !isatend(scanner)
                advance(scanner)
            end
        else
            addtoken(scanner, token_slash);
        end
    elseif char == ' ' || char == '\r' || char == '\t'
        # Ignore whitespace.
    elseif char == '\n'
        scanner.line += 1
    elseif char == '"'
        scanstring(scanner)
    elseif isdigit(char)
        scannumber(scanner)
    elseif isalpha(char)
        scanidentifier(scanner)
    else
        loxerror(scanner.runner, scanner.line, "Unexpected character.")
    end
end

function scanidentifier(scanner::Scanner)
    while isalphanumeric(peek(scanner))
        advance(scanner)
    end

    lastind = prevind(scanner.source, scanner.current)
    text = scanner.source[scanner.start:lastind]
    type = get(keywords, text, token_identifier)
    addtoken(scanner, type)
end

function scannumber(scanner::Scanner)
    while isdigit(peek(scanner))
        advance(scanner)
    end

    if peek(scanner) == '.' && isdigit(peeknext(scanner))
        # Consume the dot.
        advance(scanner)

        while isdigit(peek(scanner))
            advance(scanner)
        end
    end

    lastchar = prevind(scanner.source, scanner.current)
    val = parse(Float64, scanner.source[scanner.start:lastchar])
    addtoken(scanner, token_number, val)
end

function scanstring(scanner::Scanner)
    while peek(scanner) != '"' && !isatend(scanner)
        peek(scanner) == '\n' && (scanner.line += 1)
        advance(scanner)
    end

    if isatend(scanner)
        loxerror(scanner.runner, scanner.line, "Unterminated string.")
        return
    end

    # Get the position of the last character of the string now, before consuming the
    # closing quotation mark.
    lastchar = prevind(scanner.source, scanner.current)

    # The closing quotation mark.
    advance(scanner)

    # Trim the surrounding quotes to get the value.
    firstchar = nextind(scanner.source, scanner.start)
    addtoken(scanner, token_string, scanner.source[firstchar:lastchar])
end

function match(scanner::Scanner, expected::AbstractChar)
    isatend(scanner) && return false
    scanner.source[scanner.current] != expected && return false

    nextchar(scanner)
    true
end

function peek(scanner::Scanner)
    isatend(scanner) ? '\0' : scanner.source[scanner.current]
end

function peeknext(scanner::Scanner)
    next = nextind(scanner.source, scanner.current)
    next > length(scanner.source) ? '\0' : scanner.source[next]
end

isalpha(c) = c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z' || c == '_'

isalphanumeric(c) = isalpha(c) || isdigit(c)

isatend(scanner::Scanner) = scanner.current > ncodeunits(scanner.source)

function advance(scanner::Scanner)
    char = scanner.source[scanner.current]
    nextchar(scanner)
    char
end

function addtoken(scanner::Scanner, type::TokenType, literal=nothing)
    endind = prevind(scanner.source, scanner.current)
    lexeme = Symbol(scanner.source[scanner.start:endind])
    push!(scanner.tokens, Token(type, lexeme, literal, scanner.line))
end

end # module
