module LoxTests

using Test
using Base.Filesystem

using Lox
using Lox.Expressions
using Lox.Tokens

const expectedoutput_pattern = r"// expect: ?(.*)"
const expectederror_pattern = r"// (Error.*)"
const errorline_pattern = r"// \[((java|c) )?line (\d+)\] (Error.*)"
const expectedruntimeerror_pattern = r"// expect runtime error: (.+)"
const nontest_pattern = r"// nontest"

const syntaxerror_pattern = r"\[.*line (\d+)\] (Error.+)"
const stacktrace_pattern = r"\[line (\d+)\]"

struct TestScript
    contents::String
    expectedexitcode::Int
    expectedoutputs::Vector{Any}
    expectederrors::Vector{Any}
    runtimeerrorline::Int
    expectedruntimeerror::String
    function TestScript(name, contents)
        expectedoutputs = []
        expectederrors = []
        expectedruntimeerror = nothing
        expectedexitcode = 0
        runtimeerrorline = 0
        expectedruntimeerror = ""

        for (linenumber, line) in enumerate(split(contents, r"\r?\n"))
            # Non-test script; skip it.
            match(nontest_pattern, line) !== nothing && break

            # Expecting output.
            m = match(expectedoutput_pattern, line)
            if m !== nothing
                push!(expectedoutputs, (linenumber, m.captures[1]))
                continue
            end

            # Expecting an compile-time error.
            m = match(expectederror_pattern, line)
            if m !== nothing
                push!(expectederrors, "[$linenumber] $(m.captures[1])")
                expectedexitcode = 65
                continue
            end

            # Expecting a compile-time error (with line).
            m = match(errorline_pattern, line)
            if m !== nothing
                language = m[2]
                # Lox.jl follows the Java implementation.
                language == "java" || language === nothing || continue
                push!(expectederrors, "[$(m.captures[3])] $(m.captures[4])")
                expectedexitcode = 65
                continue
            end

            # Expecting a runtime error.
            m = match(expectedruntimeerror_pattern, line)
            if m !== nothing
                runtimeerrorline = linenumber
                expectedruntimeerror = m.captures[1]
                expectedexitcode = 70

                continue
            end

            if !isempty(expectederrors) && runtimeerrorline != 0
                error("test $name cannot expect both compile and runtime error")
            end
        end

        new(
            contents,
            expectedexitcode,
            expectedoutputs,
            expectederrors,
            runtimeerrorline,
            expectedruntimeerror,
        )
    end
end

function checkerrors(script::TestScript, io::IO)
    # Check emitted errors.
    remainingerrors = script.expectederrors
    for line in readlines(io)
        m = match(syntaxerror_pattern, line)
        if m !== nothing
            err = "[$(m.captures[1])] $(m.captures[2])"
            indices = findall(x -> x == err, remainingerrors)
            if length(indices) == 0
                error("unexpected error: $err")
            end
            for i in indices
                deleteat!(remainingerrors, i)
            end
        else
            error("unexpected output on stderr: $line")
        end
    end
    @test remainingerrors == []
end

function checkruntimeerrors(script::TestScript, io)
    lines = readlines(io)

    # Check that a runtime error has been thrown.
    @test length(lines) >= 2

    @test lines[1] == script.expectedruntimeerror

    stacklines = lines[2:end]
    m = nothing
    for stackline in stacklines
        m = match(stacktrace_pattern, stackline)
        m !== nothing && break
    end
    # Check that we got a stack trace.
    if m === nothing
        error("expected stack trace, got: ", stacklines)
    else
        linenumber = parse(Int, m.captures[1])
        @test linenumber == script.runtimeerrorline
    end
end

function checkoutput(script::TestScript, io::IO)
    outputlines = readlines(io)
    # Remove trailing last empty line.
    length(outputlines) != 0 && last(outputlines) == "" && pop!(outputlines)

    for index = 1:length(outputlines)
        line = outputlines[index]
        if index > length(script.expectedoutputs)
            error("got output '$line' when none was expected")
            continue
        end

        expected = script.expectedoutputs[index]
        if expected[2] != line
            error("expected output '$(expected[2])' on line $(expected[1]), got '$line'")
        end
    end

    index = length(outputlines) + 1
    while index < length(script.expectedoutputs)
        expected = script.expectedoutputs[index]
        error("Missing expected output '$(expected[2])' on line $(expected[1])")
        index += 1
    end
end

@testset "$(splitdir(root)[2])" for (root, dirs, files) in walkdir("lox-scripts/")
    @testset "$file" for file in files
        splitext(file)[2] == ".lox" || continue # Ignore non-Lox files.

        script = TestScript("$(splitdir(root)[2])/$file", read(joinpath(root, file), String))

        # Capture the output of the interpreter.
        io = IOBuffer(; append=true)
        ioerr = IOBuffer(; append=true)
        runner = Runner(io, ioerr)

        # Run Lox script and test its exit code.
        exitcode = runstring(runner, script.contents)
        @test exitcode == script.expectedexitcode

        if script.runtimeerrorline != 0
            checkruntimeerrors(script, ioerr)
        else
            checkerrors(script, ioerr)
        end
        checkoutput(script, io)
    end
end

include("showexpr.jl")

end # module
