# Testing printing expressions.

@testset "printing expressions" begin
    identifier_token = Token(token_identifier, :x, 1)
    identifier2_token = Token(token_identifier, :y, 1)
    plus_token = Token(token_plus, :+, 1)
    paren_token = Token(token_leftparen, Symbol("("), 1)
    equalequal_token = Token(token_equalequal, :(==), 1)
    this_token = Token(token_this, :this, 1)
    bang_token = Token(token_bang, :!, 1)
    super_token = Token(token_super, :super, 1)

    var_expr = VariableExpr(identifier_token)
    litteral42_expr = LiteralExpr(42)
    litteral729_expr = LiteralExpr(729)

    # Each expression at their most simple forms.
    expressions = [
        (AssignExpr(identifier_token, litteral42_expr), "(= x 42)"),
        (BinaryExpr(var_expr, plus_token, litteral42_expr), "(+ x 42)"),
        (CallExpr(var_expr, paren_token, []), "(call x [])"),
        (CallExpr(var_expr, paren_token, Any[litteral42_expr]), "(call x [42])"),
        (CallExpr(var_expr, paren_token, Any[litteral42_expr, litteral729_expr]), "(call x [42, 729])"),
        (GetExpr(var_expr, identifier2_token), "(. x y)"),
        (GroupingExpr(var_expr), "(group x)"),
        (litteral42_expr, "42"),
        (LiteralExpr(nothing), "nil"),
        (LiteralExpr("Lox"), "\"Lox\""),
        (LogicalExpr(var_expr, equalequal_token, litteral42_expr), "(== x 42)"),
        (SetExpr(var_expr, identifier2_token, litteral42_expr), "(.= x y 42)"),
        (ThisExpr(this_token), "this"),
        (UnaryExpr(bang_token, litteral42_expr), "(! 42)"),
        (var_expr, "x"),
        (SuperExpr(super_token, identifier_token), "(. super x)"),
    ]

    for expr in expressions
        @test sprint(print, expr[1]) == expr[2]
    end

    # And one compound expression.
    # (5.0 - (3.5 - 1.0)) + -1.0
    expr = BinaryExpr(
        GroupingExpr(
            BinaryExpr(
                LiteralExpr(5.0),
                Token(token_minus, :-, 1),
                GroupingExpr(
                    BinaryExpr(
                        LiteralExpr(3.5),
                        Token(token_minus, :-, 1),
                        LiteralExpr(1.0),
                    )
                )
            )
        ),
        Token(token_plus, :+, 1.0),
        UnaryExpr(
            Token(token_minus, :-, 1.0),
            LiteralExpr(1.0)
        )
    )
    @test sprint(print, expr) == "(+ (group (- 5.0 (group (- 3.5 1.0)))) (- 1.0))"
end
